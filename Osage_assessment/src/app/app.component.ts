import {Component, HostListener} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {count} from "rxjs/internal/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})



export class AppComponent {
    title;
    url;
    Open = 0;

    jsonData: any [] = [
    ];

    constructor(private http: HttpClient){
    }

    openDiv(title, url) {
        console.log(title, url)
        this.url = url;
        this.title = title;
    }

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        if (event.key == "Escape") {
            this.Open = 0
        }
    }

    ngOnInit(): void {
        this.http.get('https://jsonplaceholder.typicode.com/photos?albumId=3').subscribe(data => {
            for(var i = 0; i < 9; i++) {
                this.jsonData.push(data[i])
            }
        });

    }
}
